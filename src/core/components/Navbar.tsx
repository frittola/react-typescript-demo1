import React from 'react';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router';

import { IfLogged } from '../auth/IfLogged';
import { signOut } from '../auth/authentication.service';

export const Navbar: React.FC = () => {
  const history = useHistory();

  const signOutHandler = () => {
    history.push("/login");
    signOut();
  };

  return (
    <nav className="navbar navbar-expand navbar-light bg-light">
      <div className="navbar-brand">
        <Link to="/login">React Auth Example</Link>
      </div>

      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <Link className="nav-link" to="/home">Home</Link>
          </li>
          <IfLogged>
            <li className="nav-item">
              <Link className="nav-link" to="/admin">Admin</Link>
            </li>
          </IfLogged>
          <li className="nav-item">
            <Link className="nav-link" to="/settings">Settings</Link>
          </li>
          <IfLogged>
            <li className="nav-item" onClick={signOutHandler}>
              <div className="nav-link" >Logout</div>
            </li>
          </IfLogged>

        </ul>
      </div>
    </nav>
  )
};
