import React, { useEffect, useState } from 'react';
import { get } from '../core/http/http.service';

interface Product {
  id: number;
}

export const PageAdmin: React.FC = () => {
  const [products, setProducts] = useState<Product[]>([]);

  useEffect(() => {
    (async function () {
      get<Product[]>('http://localhost:3001/products')
        .then(result => setProducts(result))
    })()
  }, []);


  return <div>
    <h3>Admin Area</h3>

    Total: {products.length} products
  </div>
};
